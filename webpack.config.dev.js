import path from 'path'
import JavaScriptObfuscator from 'webpack-obfuscator';

export default {
    devtool: process.env.NODE_ENV !== 'production' ? 'inline-source-map' : '',

    entry: {
        client: path.resolve(__dirname, 'src/client.js'),
        operator: path.resolve(__dirname, 'src/operator.js')
    },

    output: {
        filename: `scripts/[name].js`,
        path: __dirname + '/build'
    },
    plugins: [
        new JavaScriptObfuscator ({
            rotateUnicodeArray: true
        })
    ]
}