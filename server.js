import webpack from 'webpack';
import config from './webpack.config.dev';

var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var port = process.env.PORT || 3000;


const compiler = webpack(config);

app.use(require('webpack-dev-middleware')(compiler, {
    noInfo: true,
    publicPath: config.output.publicPath
}));

app.use(require('express').static('public'));

app.get('/operator', function (req, res) {
    res.sendFile(__dirname + '/operator.html');
});

app.get('/client', function (req, res) {
    res.sendFile(__dirname + '/client.html');
});

app.get('/clientfromcrm', function (req, res) {
    res.sendFile(__dirname + '/clientfromcrm.html');
});


io.origins(function (origin, callback) {
    callback(null, true);
});

io.on('connection', function (socket) {

    socket.on('client_sent_mouse', (msg) => {
        io.emit('operator_sent_mouse', msg)
    });

    socket.on('client_sent_scroll', (msg) => {
        io.emit('client_sent_scroll', msg)
    });

    socket.on('client_sent_resize', (msg) => {
        io.emit('client_sent_resize', msg)
    });


    socket.on('client_sent_init', function (msg) {
        io.emit('client_sent_init', msg);
    });

    socket.on('send client markup', function (msg) {
        io.emit('send client markup', msg);
    });

    socket.on('send client markup ping', function (msg) {
        io.emit('send client markup ping', msg);
    });

//operator
    socket.on('operator_get_init', function () {
        io.emit('operator_get_init');
    });

    socket.on('operator_post_draw', (model) => {
        io.emit('client_get_operator_draw', model);
    });
});

http.listen(port,'localhost', function () {
    console.log('listening on *:' + port);
});
