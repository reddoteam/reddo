import io from 'socket.io-client';
import $ from "jquery";

$(function () {
    //это либа
    function initDraw(canvas) {
        function setMousePosition(e) {
            var ev = e || window.event; //Moz || IE
            if (ev.pageX) { //Moz
                mouse.x = ev.pageX + window.pageXOffset;
                mouse.y = ev.pageY + window.pageYOffset;
            } else if (ev.clientX) { //IE
                mouse.x = ev.clientX + document.body.scrollLeft;
                mouse.y = ev.clientY + document.body.scrollTop;
            }
        };

        var mouse = {
            x: 0,
            y: 0,
            startX: 0,
            startY: 0
        };

        var element = null;

        canvas.onmousemove = function (e) {
            setMousePosition(e);
            if (element !== null) {
                element.style.width = Math.abs(mouse.x - mouse.startX) + 'px';
                element.style.height = Math.abs(mouse.y - mouse.startY) + 'px';
                element.style.left = (mouse.x - mouse.startX < 0) ? mouse.x + 'px' : mouse.startX + 'px';
                element.style.top = (mouse.y - mouse.startY < 0) ? mouse.y + 'px' : mouse.startY + 'px';
            }
        };

        canvas.onclick = function (e) {

            if (element !== null) {

                setTimeout((element) => {
                    element.remove();
                }, 200, element);

                const iframe = document.getElementById('clientDisplayIframe');
                const iframeWindow = iframe.contentWindow;

                const model = {
                    height: element.style.height,
                    width: element.style.width,
                    x:  `${Number.parseInt(element.style.left)+iframeWindow.scrollX}px`,
                    y:  `${Number.parseInt(element.style.top)+iframeWindow.scrollY}px`
                };

                socket.emit('operator_post_draw', model);

                element = null;
                canvas.style.cursor = "default";

            } else {

                mouse.startX = mouse.x;
                mouse.startY = mouse.y;
                element = document.createElement('div');
                element.className = 'rectangle'
                element.style.left = mouse.x + 'px';
                element.style.top = mouse.y + 'px';
                canvas.appendChild(element)
                canvas.style.cursor = "crosshair";

            }
        }
    }


    var socket = io(location.host, {transports: ['websocket']});

    function replaceScriptTag(string) {
        return string.replace(/<script[^>]*>(?:(?!<\/script>)[^])*<\/script>/g, "");
    };

    function updateDom(model) {
        try {
            var doc = (new DOMParser()).parseFromString(model.markup, "text/html");
            var head = doc.getElementsByTagName("head")[0];
            var body = doc.getElementsByTagName("body")[0];

            if ($('#clientDisplayIframe').contents().find("head")[0].innerHTML.length < 1) {
                $('#clientDisplayIframe').contents().find("head")[0].innerHTML += replaceScriptTag(head.innerHTML);
            }


            $('#clientDisplayIframe').contents().find('body').html(replaceScriptTag(body.innerHTML));

            //костыль
            $('#clientDisplayIframe').contents().find('body .displayNotification').hide();
        }
        catch (e) {
            console.log(e)
        }
    }

    function handleInit(model) {
        $("#clientDisplayIframe").width(model.width);
        $("#clientDisplayIframe").height(model.height);
        $("#operatorCanvas").width(model.width);
        $("#operatorCanvas").height(model.height);

        updateDom(model);
        handleScroll(model.scroll);
        initDraw(document.getElementById('operatorCanvas'), socket);
    };

    function handleDOMMutation(model) {
        if (typeof (model.markup) === 'string') {
            updateDom(model);
        }
    };

    function handleMouseMove(model) {

        $("#clientCursor").css({top: `${model.y}px`, left: `${model.x}px`});
    };

    function handleScroll(model) {
        var iframe = $('#clientDisplayIframe').contents();
        iframe.scrollTop(model.y);
        iframe.scrollLeft(model.x);
    };

    function handleResize(model) {
        $("#clientDisplayIframe").width(model.width);
        $("#clientDisplayIframe").height(model.height);
        $("#operatorCanvas").width(model.width);
        $("#operatorCanvas").height(model.height);
    }

    socket.on('client_sent_init', handleInit);
    socket.on('send client markup ping', handleDOMMutation);
    socket.on('send client markup', handleDOMMutation);
    socket.on('operator_sent_mouse', handleMouseMove);
    socket.on('client_sent_scroll', handleScroll);
    socket.on('client_sent_resize', handleResize);


    //start connect
    socket.emit('operator_get_init');
});
