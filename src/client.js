import io from 'socket.io-client';

document.addEventListener("DOMContentLoaded", function (event) {
    var loadingEl = document.getElementsByClassName('displayNotification')[0];


    const socket = io(location.host, {transports: ['websocket']});

    function getScroll() {
        if (window.pageYOffset != undefined) {
            return {
                x: pageXOffset,
                y: pageYOffset
            };
        }
        else {
            var sx, sy, d = document, r = d.documentElement, b = d.body;
            sx = r.scrollLeft || b.scrollLeft || 0;
            sy = r.scrollTop || b.scrollTop || 0;
            return {
                x: sx,
                y: sy
            }
        }
    }

    const startSharing = () => {

// выбираем элемент
        var target = document.body;

// создаем экземпляр наблюдателя
        var observer = new MutationObserver(function (mutations) {
            mutations.forEach(function (mutation) {
                var model = {
                    markup: document.body
                };

                socket.emit('send client markup', model);
            });
        });

// настраиваем наблюдатель
        var config = {attributes: true, childList: true, characterData: true}

// передаем элемент и настройки в наблюдатель
        observer.observe(target, config);


//start mouse
        function handleMouseMove(event) {
            var model = {
                y: event.clientY,
                x: event.clientX
            };

            socket.emit('client_sent_mouse', model);
        }

//end mouse


//start scroll

        function handleScroll(event) {

            var scroll = getScroll();
            var model = {
                y: scroll.y,
                x: scroll.x
            };

            socket.emit('client_sent_scroll', model);
        }

//end scroll

// start resize

        function handleResize(event) {
            var window = event.currentTarget;

            var model = {
                height: window.innerHeight,
                width: window.innerWidth
            };

            socket.emit('client_sent_resize', model);
        }

// end resize


        target.addEventListener('mousemove', handleMouseMove);
        window.addEventListener('scroll', handleScroll);
        window.addEventListener('resize', handleResize);

        setInterval(function () {

            var markup = document.documentElement.innerHTML;

            var model = {
                markup: markup,
            };

            socket.emit('send client markup ping', model);

            model = null;
            markup = null;

        }, 1000);


        socket.on('send operator', function (model) {

            var element = null;
            element = document.createElement('div');
            element.className = 'rectangle';
            element.style.left = model.left + 'px';
            element.style.top = model.top + 'px';
            element.style.height = model.height + 'px';
            element.style.width = model.width + 'px';

            document.getElementsByTagName('body')[0].appendChild(element);

            setInterval(function () {
                element;
            }, 1000, element);
        });
    };


    const handleClientInit = () => {
        const model = {
            markup: document.documentElement.innerHTML,
            height: window.innerHeight,
            width: window.innerWidth,
            scroll: getScroll()
        };

        socket.emit('client_sent_init', model);
        loadingEl.style.visibility = "visible";

        startSharing();
    };

    const handleOperatorDraw = (model) => {

        try{
            const canvas = document.getElementById('operatorCanvas');
            const element = document.createElement('div');
            element.className = 'rectangle';
            element.style.left = model.x;
            element.style.top = model.y;
            element.style.width = model.width;
            element.style.height = model.height;

            canvas.appendChild(element);

            setTimeout((element) => {
                element.remove();
            }, 4000, element);
        }
        catch (e){
            console.log(e);
        }

    };

    socket.on('operator_get_init', handleClientInit);
    socket.on('client_get_operator_draw', handleOperatorDraw)
    //end init
});
